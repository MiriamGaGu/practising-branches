///////////////////
/* LET AND CONST */
///////////////////


// Rewrite the following line so that inner is scoped to the if statement.
if (true) {
    let inner = "Inner!";
}

// declare a variable named noChange that cannot be re-assigned and give it a value of 100.
const noChange = 100;

// Hola estoy haciendo un cambio